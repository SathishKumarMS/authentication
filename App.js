import React from 'react';
import {SafeAreaView, StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {Provider} from 'react-redux';
import Colors from './src/utils/Colors';
import Store from './src/redux/Store';
import RootNavigation from './src/navigations/RootNavigation';
import * as HelperStyles from './src/utils/HelperStyles';

const App = () => {
  return (
    <SafeAreaView style={HelperStyles.screenContainer(Colors.white)}>
      <StatusBar barStyle={'dark-content'} />
      <Provider store={Store}>
        <NavigationContainer>
          <RootNavigation />
        </NavigationContainer>
      </Provider>
    </SafeAreaView>
  );
};

export default App;
