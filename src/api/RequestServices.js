import RequestService from './Services';
import axios from 'axios';
import * as Endpoints from './Endpoints';

export const getNewsList = () => {
  // return RequestService.get(Endpoints.newsapi, null);
  return axios.get(
    'https://newsapi.org/v2/top-headlines?country=us&apiKey=21afaaaf2ae9431da48940d2c5ed52a4',
  );
};
