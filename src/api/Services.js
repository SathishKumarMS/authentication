import Fetch from './Fetch';

class HandleRequestService {
  get = (endPoint, requestData, onResponse) => {
    handleRequestHeaders('GET', endPoint, requestData, onResponse);
  };
}

const handleRequestHeaders = async (
  methodType,
  endPoint,
  requestData,
  onResponse,
) => {
  let requestHeader = {
    method: methodType,
    headers: {
      Accept: '*/*',
      'Content-Type':
        requestData instanceof FormData
          ? 'multipart/form-data'
          : 'application/json',
    },
  };

  Boolean(requestData) && [
    (requestHeader = {
      ...requestHeader,
      body:
        requestData instanceof FormData
          ? requestData
          : JSON.stringify(requestData),
    }),
  ];

  Fetch.handleFetchRequest(
    `${'https://newsapi.org/v2/top-headlines?country=us&apiKey='}${endPoint}`,
    requestHeader,
    onResponse,
  );
};
const RequestService = new HandleRequestService();

export default RequestService;
