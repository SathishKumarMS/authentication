class HandleFetchRequest {
  handleFetchRequest = (endPoint, headers, onResponse) => {
    fetch(endPoint, headers)
      .then(handleResponse)
      .then(resJson => {
        handleSuccessResponse(resJson, onResponse);
      })
      .catch(errorResponse => {
        handleErrorResponse(endPoint, errorResponse);
      });
  };
}

const handleResponse = async response => {
  const statusCode = response.status;

  const resJson = response.json();

  console.log('HANDLE RESPONSE :::: ', statusCode, resJson);

  return Promise.all([statusCode, resJson]).then(res => ({
    statusCode: res[0],
    resJson: res[1],
  }));
};

const handleSuccessResponse = (successResponse, onResponse) => {
  const response = successResponse.resJson;

  if (successResponse.statusCode == 200 || successResponse.statusCode == 201) {
    Boolean(response) && onResponse(successResponse);
  } else {
    console.log('HANDLE SUCCESS RESPONSE ERROR::::: ');
  }
};

const handleErrorResponse = (endPoint, errorResponse) => {
  console.log('HANDLE ERROR RESPONSE ::::: ', endPoint, errorResponse);
};

const FetchRequest = new HandleFetchRequest();

export default FetchRequest;
