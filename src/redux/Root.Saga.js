import {all} from 'redux-saga/effects';
import * as userSaga from './saga/UserSaga';
import * as newsSaga from './saga/NewsSaga';

export default function* rootSaga() {
  yield all([userSaga.watchUserLogin(), newsSaga.watchNews()]);
}
