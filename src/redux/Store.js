import {createStore, compose, applyMiddleware} from 'redux';
import logger from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './Root.Saga';
import rootReducer from './Root.Reducers';

let composedEnhancer;

const sagaMiddleware = createSagaMiddleware();

composedEnhancer = compose(applyMiddleware(logger, sagaMiddleware));

export const initStore = () => createStore(rootReducer, {}, composedEnhancer);

const store = initStore();

sagaMiddleware.run(rootSaga);

export default store;
