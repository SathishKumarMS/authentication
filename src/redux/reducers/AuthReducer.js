import * as Types from '../Root.Types';

const initialState = {
  userInfo: null,
  newsData: null,
};

const userReducers = (state = initialState, action) => {
  console.log('ACTION PAYLOAD IS HRE :::::::::::',action);
  switch (action.type) {
    case Types.USERDETAILS_INFO:
      return {...state, userInfo: action.payload};

    case Types.NEWS_INFO:
      return {...state, newsData: action.payload};

    default:
      return state;
  }
};

export default userReducers;
