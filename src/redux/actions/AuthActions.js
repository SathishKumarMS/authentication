import * as Types from '../Root.Types';

export const userDetailsInfor = userInfo => {
  return {
    type: Types.SAGA_USER_INFO,
    payload: userInfo,
  };
};
