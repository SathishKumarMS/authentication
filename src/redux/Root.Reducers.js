import {combineReducers} from 'redux';
import userReducers from './reducers/AuthReducer';

const appReducers = combineReducers({
  app: userReducers,
});

const rootReducer = (state, action) => {
  return appReducers(state, action);
};

export default rootReducer;
