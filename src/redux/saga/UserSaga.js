import {takeEvery, put} from 'redux-saga/effects';
import * as Types from '../Root.Types';

function* authentication(action) {
  console.log('AUTHENTICAITON::::::::::::', action);
  yield put({type: Types.USERDETAILS_INFO, payload: action.payload});
}

export function* watchUserLogin() {
  yield takeEvery(Types.SAGA_USER_INFO, authentication);
}
