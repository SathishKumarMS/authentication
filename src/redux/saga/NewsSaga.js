import {takeEvery, put, call} from 'redux-saga/effects';
// import RequestService from '../../api/Services';
// import * as Endpoints from '../../api/Endpoints';
import * as Services from '../../api/RequestServices';
import * as Types from '../Root.Types';

function* newsList(action) {
  console.log('NEWS LIST WATCH ACTION IS HERE::::::::::::', action);
  try {
    const newsData = yield call(Services.getNewsList);
    yield put({type: Types.NEWS_INFO, payload: newsData.data.articles});
  } catch (err) {
    console.log(err);
  }

  // try {
  //   const data = yield call(
  //     // RequestService.get(Endpoints.newsapi, null),
  //     action,
  //   );
  //   yield put({type: Types.NEWS_INFO, payload: data});
  // } catch (error) {
  //   yield put({type: Types.NEWS_INFO, error});
  // }
}

export function* watchNews() {
  yield takeEvery(Types.WATCH_NEWS, newsList);
}
