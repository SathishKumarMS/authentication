import React, {useCallback, useEffect, useState} from 'react';
import {View, Text, ScrollView, Image, ImageBackground} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import {connect} from 'react-redux';
import {newsDetails} from '../redux/actions/AppActions';
import firestore from '@react-native-firebase/firestore';
import * as HelperStyles from '../utils/HelperStyles';
import Card from '../container/Card';

const NewsList = props => {
  const [newsData, setNewsData] = useState(props.newsData);
  const [storeStatus, setStoreStatus] = useState(false);
  useEffect(() => {
    let isFocus = true;
    props.newsDetails();

    return () => {
      isFocus = false;
    };
  }, []);

  useEffect(() => {
    let isFocus = true;
    Boolean(props.newsData) && !storeStatus && postFirebase();

    return () => {
      isFocus = false;
    };
  }, [props.newsData]);

  const postFirebase = () => {
    let helper = [];

    let id = Math.random().toString(36).substring(1, 30);

    props.newsData.map(lol => {
      helper.push({...lol, likeStatus: false, id: id});
    });

    const helperObject = {
      newsDatas: helper,
    };

    firestore()
      .collection('news')
      .add(helperObject)
      .then(() => {
        setStoreStatus(true);
        getNewsData();
      });
  };

  const getNewsData = async () => {
    const firebase = await firestore()
      .collection('news')
      .doc('0RgCxz1UMVFYavRPM8Hl')
      .get();

    setNewsData(firebase._data.newsDatas);
  };

  const NewsItems = useCallback(() => {
    return newsData.map((lol, index) => (
      <View
        key={index}
        style={[
          HelperStyles.justView('marginHorizontal', 16),
        ]}>
        <Card
          onPress={() => {
            console.log('index', index)
            props.navigation.navigate('NewsDetails', {
              newsData: lol,
              index: index
            });
          }}
          containerStyle={{
            flexDirection: 'column',
            marginVertical: 8,
          }}>
          <View>
            <View>
              <ImageBackground
                resizeMode="cover"
                style={{width: '100%', height: 180, borderRadius: 10}}
                source={{
                  uri: lol.urlToImage,
                }}>
                <Text
                  style={[
                    HelperStyles.text(24, '800', 'white', 'center', 'none'),
                    {
                      textAlign: 'left',
                      position: 'absolute',
                      bottom: 10,
                      left: 10,
                    },
                  ]}>
                  {lol.source.name}
                </Text>
              </ImageBackground>
            </View>
            <View style={HelperStyles.margin(6, 10)}>
              <Text style={{fontSize: 18, color: 'black'}}>{lol.title}</Text>
            </View>
          </View>
        </Card>
      </View>
    ));
  }, [newsData]);

  return (
    <View style={HelperStyles.screenContainer('white')}>
      <ScrollView
        contentContainerStyle={HelperStyles.flexGrow(1)}
        keyboardShouldPersistTaps={'handled'}>
        {Boolean(newsData) &&
        Array.isArray(newsData) &&
        newsData.length != 0 ? (
          <NewsItems />
        ) : (
          <Text
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            No Data found
          </Text>
        )}
      </ScrollView>
    </View>
  );
};

const mapStateToProps = state => {
  return {
    newsData: state.app.newsData,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    newsDetails: () => {
      dispatch(newsDetails());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsList);
