import React, {useCallback, useState} from 'react';
import {View, Text, ScrollView, ImageBackground} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import firestore from '@react-native-firebase/firestore';
import * as HelperStyles from '../utils/HelperStyles';
import Card from '../container/Card';

const Favorite = () => {
  const [favorite, setFavorite] = useState(null);

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;
      getFavoriteNews();
      return () => {
        isFocus = false;
      };
    }, []),
  );

  const Favorite = () => {
    return favorite.map((lol, index) => (
      <View
        key={index}
        style={[
          HelperStyles.justView('marginHorizontal', 16),
        ]}>
        <Card
          onPress={() => {
            console.log('index', index);
            // props.navigation.navigate('NewsDetails', {
            //   newsData: lol,
            //   index: index,
            // });
          }}
          containerStyle={{
            flexDirection: 'column',
            marginVertical: 8,
          }}>
          <View>
            <View>
              <ImageBackground
                resizeMode="cover"
                style={{width: '100%', height: 180, borderRadius: 10}}
                source={{
                  uri: lol.urlToImage,
                }}>
                <Text
                  style={[
                    HelperStyles.text(24, '800', 'white', 'center', 'none'),
                    {
                      textAlign: 'left',
                      position: 'absolute',
                      bottom: 10,
                      left: 10,
                    },
                  ]}>
                  {lol.source.name}
                </Text>
              </ImageBackground>
            </View>
            <View style={HelperStyles.margin(6, 10)}>
              <Text style={{fontSize: 18, color: 'black'}}>{lol.title}</Text>
            </View>
          </View>
        </Card>
      </View>
    ));
  };

  const getFavoriteNews = async () => {
    const favoriteNews = await firestore().collection('Favorites').get();

    let helperArray = [];

    favoriteNews.docs.map(lol => helperArray.push(lol._data));

    setFavorite(helperArray);
  };

  return (
    <View style={HelperStyles.screenContainer('white')}>
      <ScrollView
        contentContainerStyle={HelperStyles.flexGrow(1)}
        keyboardShouldPersistTaps={'handled'}>
        {Boolean(favorite) &&
        Array.isArray(favorite) &&
        favorite.length != 0 ? (
          <Favorite />
        ) : (
          <Text
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}></Text>
        )}
      </ScrollView>
    </View>
  );
};

export default Favorite;
