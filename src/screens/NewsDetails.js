import React, {useLayoutEffect, useState} from 'react';
import {View, TouchableOpacity, ScrollView} from 'react-native';
import {WebView} from 'react-native-webview';
import firestore from '@react-native-firebase/firestore';
import Icon from 'react-native-vector-icons/AntDesign';
import * as HelperStyles from '../utils/HelperStyles';

const NewsDetails = props => {
  const newsUrl = props.route.params.hasOwnProperty('newsData')
    ? props.route.params.newsData.url
    : null;

  const newsTitle = props.route.params.hasOwnProperty('newsData')
    ? props.route.params.newsData.source.name
    : null;

  const newsData = props.route.params.hasOwnProperty('newsData')
    ? props.route.params.newsData
    : null;

  const [likeStatus, setLikeStatus] = useState(false);

  useLayoutEffect(() => {
    props.navigation.setOptions({
      headerRight: () => renderHeaderRight(),
      title: newsTitle,
    });
  });

  const getLikeState = () => {
    if (likeStatus) {
      firestore()
        .collection('news')
        .doc('0RgCxz1UMVFYavRPM8Hl')
        .update({
          newsDatas: firestore.FieldValue.arrayUnion({
            ...newsData,
            likeStatus: true,
          }),
        })
        .then(() => {
          console.log('Successfully update data in firestore');
        });

      return {icon: 'like1', color: '#4D89C1'};
    } else {
      firestore()
        .collection('news')
        .doc('0RgCxz1UMVFYavRPM8Hl')
        .update({
          newsDatas: firestore.FieldValue.arrayRemove({
            ...newsData,
            likeStatus: true,
          }),
        })
        .then(() => {
          console.log('Successfully update data in firestore');
        });
      return {icon: 'like2', color: 'black'};
    }
  };

  const addLikeDataInFirebase = news => {
    firestore()
      .collection('news')
      .add(news)
      .then(() => {
        console.log('User added!');
      });
  };

  const storeFavorites = () => {
    firestore()
      .collection('Favorites')
      .add(newsData)
      .then(() => {
        console.log('User added!');
      });
  };

  const renderHeaderRight = () => {
    return (
      <TouchableOpacity
        onPress={async () => {
          setLikeStatus(!likeStatus);

          storeFavorites();
          // const firebase = await firestore()
          //   .collection('news')
          //   .get();

          // console.log(
          //   'fire store all data:::::::::::: ',
          //   firebase.docs[0].ref._documentPath._parts[1],
          // );
        }}
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          paddingHorizontal: 4,
          paddingVertical: 4,
        }}>
        <Icon
          name={getLikeState().icon}
          size={22}
          color={getLikeState().color}
        />
      </TouchableOpacity>
    );
  };

  return (
    <View style={HelperStyles.screenContainer('white')}>
      <ScrollView
        contentContainerStyle={HelperStyles.flexGrow(1)}
        keyboardShouldPersistTaps={'handled'}>
        <WebView source={{uri: newsUrl}} style={{marginTop: 10}} />
      </ScrollView>
    </View>
  );
};

export default NewsDetails;
