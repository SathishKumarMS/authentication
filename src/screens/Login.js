import React, {useCallback, useState} from 'react';
import {TouchableOpacity, View, Text} from 'react-native';
import {
  GoogleSignin,
  statusCodes,
} from '@react-native-google-signin/google-signin';
import remoteConfig from '@react-native-firebase/remote-config';
import {connect} from 'react-redux';
import {useFocusEffect} from '@react-navigation/core';
import {userDetailsInfor} from '../redux/actions/AuthActions';
import Button from '../components/Button';
import TextInputBox from '../components/TextInput';
import Labels from '../utils/Strings';
import auth from '@react-native-firebase/auth';
import Card from '../container/Card';
import Colors from '../utils/Colors';
import Icon from 'react-native-vector-icons/Ionicons';
import * as HelperStyles from '../utils/HelperStyles';
import * as Helpers from '../utils/Helpers';

const Login = props => {
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  const [backgroundColor, setBackgroundColor] = useState('none');

  //Error Variables
  const [emailAddressError, setEmailAddressError] = useState(false);
  const [emailAddressInvalidError, setEmailAddressInvalidError] =
    useState(false);
  const [passwordError, setPasswordError] = useState(false);
  const [passwordInvalidError, setPasswordInvalidError] = useState(false);

  //Google configuration
  GoogleSignin.configure({
    scopes: ['https://www.googleapis.com/auth/drive.readonly'],
    webClientId:
      '1004750211220-rls5hs2d98bcge5e7r59tl584bpqfi0q.apps.googleusercontent.com',
    iosClientId: '',
    androidClientId:
      '477435735075-mbknjtv5aidfi0k1aa33df1tthu1sorp.apps.googleusercontent.com',
  });

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      config();

      return () => {
        isFocus = false;
      };
    }, []),
  );

  const config = () => {
    const backgroundColor = remoteConfig().getValue('background_color');

    setBackgroundColor(backgroundColor._value);

    console.log('Background color::::::::::: ', backgroundColor);
  };

  const handleGoogleSign = async () => {
    try {
      await GoogleSignin.hasPlayServices();

      const googleUserInfo = await GoogleSignin.signIn();

      console.log('Google user info ::::', googleUserInfo);

      const userInfo = {
        email: googleUserInfo.user.email,
        name: googleUserInfo.user.name,
      };

      props.userDetailsInfor(userInfo);

      handleReset();

      props.navigation.navigate('Home');
    } catch (error) {
      switch (error.code) {
        case statusCodes.SIGN_IN_CANCELLED:
          console.log(
            'USER CANCEL THE LOGIN :::: ',
            statusCodes.SIGN_IN_CANCELLED,
          );
          break;
        case statusCodes.IN_PROGRESS:
          console.log('USER PROCESSING THE LOGIN::: ', statusCodes.IN_PROGRESS);
          break;

        case statusCodes.PLAY_SERVICES_NOT_AVAILABLE:
          console.log(
            'PLAY SERVICE NOT AVAILABLE OR OUTDATED :::: ',
            statusCodes.PLAY_SERVICES_NOT_AVAILABLE,
          );
          break;

        default:
          break;
      }
    }
  };

  const handleEmailAddress = txt => {
    const isValidEmailAddress = Helpers.validateEmail(txt);

    emailAddressError && setEmailAddressError(false);

    setEmail(Boolean(txt) ? txt : null);

    setEmailAddressInvalidError(isValidEmailAddress);
  };

  const handlePassword = txt => {
    const isValidPassword = Helpers.validatePassword(txt);

    passwordError && setPasswordError(false);

    setPassword(Boolean(txt) ? txt : null);

    setPasswordInvalidError(isValidPassword);
  };

  const handleSignIn = async () => {
    if (checkSignIn()) {
      try {
        await auth().createUserWithEmailAndPassword(email, password);

        // auth().onAuthStateChanged(user => {
        //   console.log('ON AUTH STATE CHANGED::::', user);
        // });

        let userInfo = {
          email: email,
          name: email.slice(0, email.indexOf('@')),
        };

        props.userDetailsInfor(userInfo);

        alert(`${'Login Successfully'} ${email.slice(0, email.indexOf('@'))}`);

        handleReset();
        props.navigation.navigate('Home');
      } catch {
        console.log('error');
      }
    } else {
      handleErrorValidation();
    }
  };

  const handleLogin = async () => {
    if (checkSignIn()) {
      try {
        await auth().signInWithEmailAndPassword(email, password);
        let userInfo = {
          email: email,
          name: email.slice(0, email.indexOf('@')),
        };

        props.userDetailsInfor(userInfo);
        handleReset();
        props.navigation.navigate('Home');
      } catch {
        alert('Sorry! Unable to verify the registered mail');
      }
    } else {
      handleErrorValidation();
    }
  };

  const checkSignIn = () => {
    if (
      Boolean(email) &&
      !emailAddressInvalidError &&
      Boolean(password) &&
      !passwordInvalidError
    ) {
      return true;
    } else {
      return false;
    }
  };

  const handleErrorValidation = () => {
    setEmailAddressError(Boolean(email) ? false : true);
    setPasswordError(Boolean(password) ? false : true);
  };

  const handleReset = () => {
    setEmail(null);
    setPassword(null);
    setEmailAddressError(false);
    setEmailAddressInvalidError(false);
    setPasswordError(false);
    setPasswordInvalidError(false);
  };

  return (
    <View
      style={[
        HelperStyles.screenCenter('center', 'center'),
        {backgroundColor: backgroundColor},
      ]}>
      <View style={HelperStyles.justView('width', '100%')}>
        <Card containerStyle={HelperStyles.margin(16, 8)}>
          <TextInputBox
            autoCapitalize={'none'}
            editable={true}
            keyboardType={'email-address'}
            textContentType={'emailAddress'}
            updateMasterState={txt => {
              handleEmailAddress(txt);
            }}
            placeholder={'Enter your Email'}
            value={email}
          />
        </Card>
        {emailAddressError && (
          <Text
            style={[
              HelperStyles.errorText,
              HelperStyles.justView('marginHorizontal', 16),
            ]}>
            {Labels.emailAddressError}
          </Text>
        )}
        {Boolean(email) && emailAddressInvalidError && (
          <Text
            style={[
              HelperStyles.errorText,
              HelperStyles.justView('marginHorizontal', 16),
            ]}>
            {Labels.emailAddressInvalidError}
          </Text>
        )}
        <Card containerStyle={HelperStyles.margin(16, 8)}>
          <TextInputBox
            autoCapitalize={'none'}
            editable={true}
            ispassword={true}
            keyboardType={'default'}
            showPasswordIcon={true}
            textContentType={'password'}
            updateMasterState={txt => {
              handlePassword(txt);
            }}
            placeholder={'Enter your Password'}
            value={password}
          />
        </Card>
        {passwordError && (
          <Text
            style={[
              HelperStyles.errorText,
              HelperStyles.justView('marginHorizontal', 16),
            ]}>
            {Labels.passwordError}
          </Text>
        )}
        {Boolean(password) && passwordInvalidError && (
          <Text
            style={[
              HelperStyles.errorText,
              HelperStyles.justView('marginHorizontal', 16),
            ]}>
            {password.length < 6
              ? Labels.passwordCountError
              : Labels.passwordInvalidError}
          </Text>
        )}
        <Button
          containerStyle={HelperStyles.margin(16, 12)}
          label={'Sign In'}
          onPress={() => {
            handleSignIn();
          }}
        />
        <Button
          containerStyle={HelperStyles.margin(16, 12)}
          label={'Login'}
          onPress={() => {
            handleLogin();
          }}
        />
        <TouchableOpacity
          onPress={() => {
            handleGoogleSign();
          }}
          style={HelperStyles.justifyContentCenteredView('center')}>
          <Icon name="logo-google" size={30} color={Colors.aliceBlue} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    userDetailsInfor: userDetails => {
      dispatch(userDetailsInfor(userDetails));
    },
  };
};

export default connect(null, mapDispatchToProps)(Login);
