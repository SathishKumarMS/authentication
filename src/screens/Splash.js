import React, {useCallback} from 'react';
import {View, Text} from 'react-native';
import {useFocusEffect} from '@react-navigation/core';
import Messaging from '@react-native-firebase/messaging';
import * as HelperStyles from '../utils/HelperStyles';

const Splash = props => {
  useFocusEffect(
    useCallback(() => {
      setTimeout(() => {
        props.navigation.navigate('Login');
      }, 1000);
    }, []),
  );

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      initPushNotification();

      return () => {
        isFocus = false;
      };
    }, []),
  );

  const initPushNotification = () => {
    //Check Permission
    checkPermission();

    //Background Notification
    backgroundNotification();

    closedNotification();
  };

  const checkPermission = async () => {
    const permissionStatus = await Messaging().hasPermission();

    console.log('PERMISSION STATUS:::: ', permissionStatus);

    if (permissionStatus) {
      getToken();
    } else {
      requestPermission();
    }
  };

  const getToken = async () => {
    const fcmToken = await Messaging().getToken();

    console.log('PUSH NOTIFICATION TOKEN::: ', fcmToken);
  };

  const requestPermission = async () => {
    try {
      await Messaging().requestPermission();

      getToken();
    } catch (err) {
      console.log('REQUEST PERMISSION ERROR', err);
    }
  };

  const backgroundNotification = () => {
    Messaging().onNotificationOpenedApp(messageData => {
      console.log('BACKGROUND NOTIFICATION::: ', messageData);
    });
  };

  const closedNotification = async () => {
    Messaging()
      .getInitialNotification()
      .then(messageData => {
        console.log('CLOSED NOTIFICATION::: ', messageData);
      });
  };

  return (
    <View style={HelperStyles.screenCenter('center', 'center')}>
      <Text style={HelperStyles.text(20, '600', 'black', 'center', 'none')}>
        Splash
      </Text>
    </View>
  );
};

export default Splash;
