import React from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import NewsList from './NewsList';
import Favorite from './Favorite';
import Info from './Info';
const Tab = createMaterialTopTabNavigator();

const Home = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name="NewsList" component={NewsList} />
      <Tab.Screen name="Favorite" component={Favorite} />
      <Tab.Screen name="Info" component={Info} />
    </Tab.Navigator>
  );
};

export default Home;
