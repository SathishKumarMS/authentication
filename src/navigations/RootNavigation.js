import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import AuthNavigation from './AuthNavigation';

const RootNavigation = () => {
  const Stack = createNativeStackNavigator();
  return (
    <Stack.Navigator
      initialRouteName={'Home'}
      screenOptions={() => ({
        headerBackTitleVisible: false,
        headerTitleAlign: 'center',
        headerShadowVisible: false,
        headerStyle: 'black',
      })}>
      {AuthNavigation()}
    </Stack.Navigator>
  );
};

export default RootNavigation;
