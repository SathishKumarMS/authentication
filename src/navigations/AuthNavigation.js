import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Splash from '../screens/Splash';
import Login from '../screens/Login';
import Home from '../screens/Home';
import NewsDetails from '../screens/NewsDetails';

const AuthNavigation = () => {
  const Stack = createNativeStackNavigator();
  return (
    <Stack.Group>
      <Stack.Screen
        name="Splash"
        component={Splash}
        options={() => ({headerShown: false})}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={() => ({headerShown: false})}
      />
      <Stack.Screen
        name="Home"
        component={Home}
        options={() => ({headerShown: false})}
      />
      <Stack.Screen
        name="NewsDetails"
        component={NewsDetails}
      />
    </Stack.Group>
  );
};

export default AuthNavigation;
