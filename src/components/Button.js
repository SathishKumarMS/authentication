import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Colors from '../utils/Colors';
import Styles from '../styles/Button';
import * as HelperStyles from '../utils/HelperStyles';

const Button = ({
  disabled = false,
  onPress = () => {},
  label = 'Button',
  containerStyle,
}) => {
  const handleOnPress = () => {
    onPress();
  };

  const renderButton = () => {
    return (
      <View style={[HelperStyles.screenCenter('center', 'center')]}>
        <View style={HelperStyles.justifyContentCenteredView('center')}>
          <View
            style={[
              HelperStyles.screenCenter('center', 'center'),
              HelperStyles.justView('flexDirection', 'row'),
            ]}>
            <Text
              style={[
                HelperStyles.text(16, '600', 'black', 'center', 'capitalize'),
              ]}>
              {label}
            </Text>
          </View>
        </View>
      </View>
    );
  };

  return (
    <TouchableOpacity
      disabled={disabled}
      style={[
        Styles.buttonContainer,
        containerStyle,
        {
          backgroundColor: 'aliceblue',
          borderColor: Colors.white,
          color: Colors.skyBlue,
          borderRadius: 4,
          borderWidth: 0.5,
        },
      ]}
      onPress={() => {
        handleOnPress();
      }}>
      {renderButton()}
    </TouchableOpacity>
  );
};

export default Button;
