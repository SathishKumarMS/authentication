import React, {useState} from 'react';
import {View, TouchableOpacity, TextInput} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Colors from '../utils/Colors';
import Styles from '../styles/TextInput';
import * as HelperStyles from '../utils/HelperStyles';

const TextInputBox = ({
  autoCapitalize = 'none',
  editable = true,
  ispassword = false,
  keyboardType = 'default',
  textContentType = 'none',
  showPasswordIcon = false,
  placeholder = '',
  updateMasterState = () => {},
  value = null,
}) => {
  const [passwordVisibility, setPasswordVisibility] = useState(
    ispassword ? !ispassword : true,
  );
  return (
    <View style={HelperStyles.justView('width', '100%')}>
      <View style={[HelperStyles.screenSubContainer]}>
        <View
          style={[
            HelperStyles.justView('flexDirection', 'row'),
            HelperStyles.justView('justifyContent', 'space-between'),
          ]}>
          <TextInput
            autoCapitalize={autoCapitalize}
            keyboardType={keyboardType}
            textContentType={textContentType}
            editable={editable}
            secureTextEntry={!passwordVisibility}
            onChangeText={txt => {
              updateMasterState(txt);
            }}
            value={value}
            placeholder={placeholder}
            style={Styles.textInputView}
          />
          {(showPasswordIcon || (Boolean(value) && ispassword)) && (
            <View
              style={[
                HelperStyles.flex(0.1),
                HelperStyles.justifyContentCenteredView('center'),
              ]}>
              <TouchableOpacity
                onPress={() => {
                  setPasswordVisibility(!passwordVisibility);
                }}>
                <MaterialIcons
                  name={passwordVisibility ? 'visibility-off' : 'visibility'}
                  size={24}
                  color={Colors.lightText}
                  style={HelperStyles.justView('top', 2)}
                />
              </TouchableOpacity>
            </View>
          )}
        </View>
      </View>
    </View>
  );
};

export default TextInputBox;
