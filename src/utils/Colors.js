const colors = {
  white: '#FFFFFF',
  skyBlue:'#C7ECDE ',
  aliceBlue:'#4F8EF7',
  lightText: '#98A5C4',
  shadow: '#333333',
};

export default colors;
