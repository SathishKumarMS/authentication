export const screenContainer = backgroundColorValue => {
  return {
    flex: 1,
    backgroundColor: backgroundColorValue,
  };
};

export const flex = flexValue => {
  return {
    flex: flexValue,
  };
};


export const flexGrow = flexValue => {
  return {
    flexGrow: flexValue,
  };
};

export const screenCenter = (justifyContantValue, alignItemValue) => {
  return {
    flex: 1,
    justifyContent: justifyContantValue,
    alignItems: alignItemValue,
  };
};

export const text = (
  sizeValue,
  weightValue,
  colorValue,
  alignValue,
  textTransformValue,
) => {
  return {
    fontSize: sizeValue,
    fontWeight: weightValue,
    color: colorValue,
    textAlign: alignValue,
    textTransform: textTransformValue,
  };
};

export const screenSubContainer = {
  marginHorizontal: 16,
  marginVertical: 16,
};

export const justView = (styleLabel, styleValue) => {
  return {
    [styleLabel]: styleValue,
  };
};

export const errorText = {
  color: 'red',
  fontSize: 12,
  fontWeight: '400',
  marginHorizontal: 4,
  marginVertical: 4,
};


export const margin = (marginHorizontalValue, marginVerticalValue) => {
  return {
    marginHorizontal: marginHorizontalValue,
    marginVertical: marginVerticalValue,
  };
};

export const justifyContentCenteredView = justifyContentValue => {
  return {
    justifyContent: justifyContentValue,
    alignItems: 'center',
  };
};
