const strings = {
  emailAddressError: 'Email address is required!',
  emailAddressInvalidError: 'Email address is invalid!',
  passwordError: 'Password is required!',
  passwordCountError: 'Password must have atleast 8 characters!',
  passwordInvalidError: 'Password must be alphanumeric!',
};

export default strings;
