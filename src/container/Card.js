import React from 'react';
import {TouchableOpacity} from 'react-native';
import Styles from '../styles/Card';
import Colors from '../utils/Colors';
import * as HelperStyles from '../utils/HelperStyles';

const Card = ({
  containerStyle,
  children,
  disabled = false,
  onPress = () => {},
}) => {
  return (
    <TouchableOpacity
      disabled={disabled}
      onPress={() => {
        onPress();
      }}
      style={[
        HelperStyles.justView('backgroundColor', Colors.white),
        Styles.cardContainer,
        containerStyle,
      ]}>
      {children}
    </TouchableOpacity>
  );
};

export default Card;
