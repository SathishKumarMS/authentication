import {StyleSheet} from 'react-native';

const TextInput = StyleSheet.create({
  textInputView: {
    // flex:1,
    // height: 50,
    // borderWidth: 1,
    // padding: 10,
    flex: 1,
    fontSize: 16,
    fontWeight: '400',
    paddingHorizontal: 4,
    borderRadius: 6,
    top: 0,
  },
});

export default TextInput;
